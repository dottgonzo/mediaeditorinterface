
import * as $ from 'jquery'
require('ion-rangeslider')

import * as videojs from 'video.js'
import * as unicoid from 'unicoid'
import * as fileinfo from 'filenameinfo'
const moment: any = require("moment")
require("moment-duration-format")

const clocksvg = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%" height="100%" viewBox="0 0 640 640"><g id="icomoon-ignore">\
</g>\
<path d="M320 25.6c-162.592 0-294.4 131.84-294.4 294.4 0 162.592 131.808 294.4 294.4 294.4s294.4-131.808 294.4-294.4c0-162.592-131.808-294.4-294.4-294.4zM320 550.4c-127.264 0-230.4-103.168-230.4-230.4s103.136-230.4 230.4-230.4 230.4 103.168 230.4 230.4-103.136 230.4-230.4 230.4zM342.4 153.6h-44.8v175.68l108.96 108.96 31.68-31.68-95.84-95.84z"></path>\
</svg>'



export default function (config?: { el?: string }) {
  window['videojsvids'] = []

  function secondsToTime(ss): string {
    return moment.duration(ss, 'seconds').format('hh:mm:ss.SSS', { trim: false })
  }

  function timeToSeconds(timestring): number {
    const timearray = timestring.split('.')[0].split(':')
    return (parseInt(timearray[0]) * 3600) + (parseInt(timearray[1]) * 60) + parseInt(timearray[2])
  }
  window['videoeditor'] = function (videoid) {
    let d: any = false
    for (let i = 0; i < window['videojsvids'].length; i++) {

      if (window['videojsvids'][i].uid === videoid || window['videojsvids'][i].uid.split('_videojs')[0] === videoid) {
        d = window['videojsvids'][i]
      }

    }
    if (d) {
      return d
    } else {
      return false
    }
  }

  window['cuevid'] = function (uid, direction) {
    const time = parseInt(window['videoeditor'](uid).data.time)
    let targetslider: any
    let options
    let videodata: any = false
    for (let i = 0; i < window['videojsvids'].length; i++) {

      if (window['videojsvids'][i].uid === uid) {
        videodata = window['videojsvids'][i]
      }

    }


    if (!videodata) throw Error('data error')

    switch (direction) {
      case 'left':
        if (videodata.data.to < time) return
        targetslider = $('#' + uid + '_sliderleft')
        options = { from: time }
        videodata.data.from = time
        break
      case 'right':
        if (videodata.data.from > time) return
        targetslider = $('#' + uid + '_sliderright')
        options = { to: time }
        videodata.data.to = time
        break
    }
    videodata.data.duration = videodata.data.to - videodata.data.from
    $('#' + uid + '_time').text(secondsToTime(videodata.data.duration))
    targetslider.val(secondsToTime(time))
    $('#' + uid + '_slider').data("ionRangeSlider").update(options)

  }


  let el = 'videoeditor'
  if (config) {

    if (config.el) el = config.el

  }

  $.each($("." + el), function () {

    let videoid
    if ($(this).attr('id')) {
      videoid = $(this).attr('id') + '_videojs'
    } else {
      videoid = unicoid.uniqueid(7)
    }



    const sources = $(this).attr('videouri').split(';')
    let videoSourceNode = ''

    for (let i = 0; i < sources.length; i++) {
      const source = sources[i]

      const sourceType = fileinfo.filenameinfo(source.split('?')[0]).contentType
      videoSourceNode += '<source src="' + source + '" type="' + sourceType + '">'

    }
    let player;
    if ($(this).html()) {
      $(this).html('<video style="width:100%" class="video-js" id="' + videoid + '" controls preload="auto" data-setup="{}">' + videoSourceNode + '</video>')
      videojs(videoid).dispose()    
      player = videojs(videoid)
    } else {
      $(this).html('<video style="width:100%" class="video-js" id="' + videoid + '" controls preload="auto" data-setup="{}">' + videoSourceNode + '</video>')
      player = videojs(videoid)
    }

    $(this).append('<div style="text-align:center;margin:10px auto 10px auto"><input id="' + videoid + '_sliderleft" style="float:left;margin-right:10px;width:100px" type="text" /><a style="float:left" href="javascript:void(0)" onclick="cuevid(\'' + videoid + '\',\'left\')">cue</a><span style="width:15px;height:15px;display:inline-block">' + clocksvg + '</span> <span id="' + videoid + '_time"></span></span><input id="' + videoid + '_sliderright" style="float:right;margin-left:10px;width:100px" type="text" /><a style="float:right" href="javascript:void(0)" onclick="cuevid(\'' + videoid + '\',\'right\')">cue</a></div>')

    $(this).append('<input type="text" id="' + videoid + '_slider" name="' + videoid + '_slidername" value="" />')


    player.on('loadedmetadata', function () {


      $('#' + videoid).css('height', ($('#' + videoid).width() * this.videoHeight()) / this.videoWidth() + 'px')





      const duration = player.duration().toFixed()

      const initfrom = 0
      const initto = player.duration().toFixed()



      $('#' + videoid + '_sliderleft').val(secondsToTime(initfrom))
      $('#' + videoid + '_sliderright').val(secondsToTime(parseInt(player.duration())))
      $('#' + videoid + '_time').text(secondsToTime(parseInt(player.duration())))


      $('#' + videoid + '_sliderleft').on('keyup', function () {
        const timeval = $('#' + videoid + '_sliderleft').val()
        if (timeval.split('.').length === 2 && timeval.split(':').length === 3) {
          let valid = true
          const timearray = timeval.split('.')[0].split(':')

          for (let i = 0; i < timearray.length; i++) {
            const piece = timearray[i]
            if (!(piece.length === 2 && parseInt(piece) >= 0 && parseInt(piece) < 60)) {
              valid = false
            }
          }
          if (valid && timeToSeconds(timeval) < window['videoeditor'](videoid).data.to) {
            $('#' + videoid + '_slider').data("ionRangeSlider").update({ from: timeToSeconds(timeval) })
            window['videoeditor'](videoid).data.from = timeToSeconds(timeval)
            window['videoeditor'](videoid).data.duration = window['videoeditor'](videoid).data.to - window['videoeditor'](videoid).data.from
            $('#' + videoid + '_time').text(secondsToTime(window['videoeditor'](videoid).data.duration))

          }
        }
      })
      $('#' + videoid + '_sliderright').on('keyup', function () {
        const timeval = $('#' + videoid + '_sliderright').val()
        if (timeval.split('.').length === 2 && timeval.split(':').length === 3) {
          let valid = true
          const timearray = timeval.split('.')[0].split(':')

          for (let i = 0; i < timearray.length; i++) {
            const piece = timearray[i]
            if (!(piece.length === 2 && parseInt(piece) >= 0 && parseInt(piece) < 60)) {
              valid = false
            }
          }
          if (valid && timeToSeconds(timeval) > window['videoeditor'](videoid).data.from) {
            $('#' + videoid + '_slider').data("ionRangeSlider").update({ to: timeToSeconds(timeval) })
            window['videoeditor'](videoid).data.to = timeToSeconds(timeval)
            window['videoeditor'](videoid).data.duration = window['videoeditor'](videoid).data.to - window['videoeditor'](videoid).data.from
            $('#' + videoid + '_time').text(secondsToTime(window['videoeditor'](videoid).data.duration))

          }
        }

      })


      window['videojsvids'].push({ uid: videoid, sources: sources, duration: duration, data: { time: 0, from: initfrom, to: initto, duration: duration } })
      const thisvideodata = window['videoeditor'](videoid)


      $('#' + videoid + '_slider').ionRangeSlider({
        type: "double",
        grid: true,
        min: 0,
        max: duration,
        from: initfrom,
        to: initto,
        prefix: "Time: ",
        onChange: function (a) {
          $('#' + videoid + '_sliderleft').val(secondsToTime(a.from))
          $('#' + videoid + '_sliderright').val(secondsToTime(a.to))
          thisvideodata.data.from = a.from
          thisvideodata.data.to = a.to
          thisvideodata.data.duration = a.to - a.from
          $('#' + videoid + '_time').text(secondsToTime(parseInt(thisvideodata.data.duration)))

        }
      })
      this.on('timeupdate', function () {
        thisvideodata.data.time = this.currentTime()
      })

    });




  })


}